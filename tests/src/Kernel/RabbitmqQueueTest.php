<?php

namespace Drupal\Tests\purger_extended_queues\Kernel\Queue;

use Drupal\Core\Site\Settings;

/**
 * Tests \Drupal\purge\Plugin\Purge\Queue\DatabaseQueue.
 *
 * @group purger_extended_queues
 */
class RabbitmqQueueTest extends PluginTestBase {
  /**
   * {@inheritDoc}
   */
  protected static $modules = ['purge', 'purger_extended_queues', 'rabbitmq'];

  /**
   * {@inheritDoc}
   */
  public function setUp($switch_to_memory_queue = TRUE): void {
    parent::setUp();
    $settings = Settings::getAll();
    $settings['rabbitmq_credentials'] = [];
    $settings['rabbitmq_credentials']['default'] = [
      'host' => getenv('RABBITMQ_HOST'),
      'port' => getenv('RABBITMQ_PORT') ?: 5672,
      'username' => getenv('RABBITMQ_USER') ?: 'guest',
      'password' => getenv('RABBITMQ_PASSWORD') ?: 'guest',
      'vhost' => getenv('RABBITMQ_VHOST') ?: '/',
      'options' => [
        'connection_timeout' => 5,
        'read_write_timeout' => 5,
      ],
    ];
    new Settings($settings);
    try {
      $this->setUpQueuePlugin();
      $this->queue->numberOfItems();
    }
    catch (\Throwable $e) {
      echo $e->getMessage();
      $this->markTestSkipped('requires rabbitmq');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function testPaging(): void {
    // Not supported.
  }

  /**
   * {@inheritdoc}
   */
  protected $pluginId = 'rabbitmq';

}
