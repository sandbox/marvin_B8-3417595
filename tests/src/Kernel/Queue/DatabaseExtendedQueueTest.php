<?php

namespace Drupal\Tests\purger_extended_queues\Kernel\Queue;

/**
 * Tests \Drupal\purge\Plugin\Purge\Queue\DatabaseQueue.
 *
 * @group purger_extended_queues
 */
class DatabaseExtendedQueueTest extends PluginTestBase {

  /**
   * {@inheritdoc}
   */
  protected $pluginId = 'database_extended';

  /**
   * {@inheritDoc}
   */
  public function setUp($switch_to_memory_queue = TRUE): void {
    parent::setUp();
    $this->setUpQueuePlugin();
  }

}
