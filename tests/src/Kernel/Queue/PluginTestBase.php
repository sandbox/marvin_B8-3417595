<?php

namespace Drupal\Tests\purger_extended_queues\Kernel\Queue;

use Drupal\purge\Plugin\Purge\Queue\PluginManager;
use Drupal\Tests\purger_extended_queues\Kernel\KernelPluginManagerTestBase;

/**
 * Provides a abstract test class to aid thorough tests for queue plugins.
 *
 * @see \Drupal\purge\Plugin\Purge\Queue\QueueInterface
 */
abstract class PluginTestBase extends KernelPluginManagerTestBase {

  /**
   * The plugin ID of the queue plugin being tested.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * {@inheritdoc}
   */
  protected $pluginManagerClass = PluginManager::class;

  /**
   * The queue plugin being tested.
   *
   * @var null|\Drupal\purge\Plugin\Purge\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Set up the test. (The return value has changed)
   */
  // @codingStandardsIgnoreStart
  public function setUp($switch_to_memory_queue = TRUE): void {
    parent::setUp($switch_to_memory_queue);
  }
  // @codingStandardsIgnoreEnd

  /**
   * Load the queue plugin and make $this->queue available.
   */
  protected function setUpQueuePlugin(): void {
    if (is_null($this->queue)) {
      $this->queue = $this->pluginManager->createInstance($this->pluginId);
      $this->assertNull($this->queue->createQueue());
    }
  }

  /**
   * Test the data integrity of data stored in the queue.
   */
  public function testDataStorageIntegrity(): void {
    $samples = [
      'a' => 'string',
      'b' => 'StrinG with Capitalization',
      'c' => 1,
      'd' => -1500,
      'e' => 0.1500,
      'f' => -99999,
      'g' => NULL,
      'h' => FALSE,
      'i' => TRUE,
    ];

    // Test if we get back the exact same thing if we store it as scalar value.
    foreach ($samples as $key => $sample) {
      $this->queue->createItem($sample);
      $reference = $this->queue->claimItem(3600);
      $this->assertIsObject($reference);
      $this->assertSame($reference->data, $sample, 'The example value is ' . $key);
    }

    // Test that we get the same data back by storing it in an object.
    $this->queue->createItem($samples);
    $reference = $this->queue->claimItem(3600);
    $this->assertIsObject($reference);
    $this->assertSame($reference->data, $samples);

    $this->queue->deleteQueue();
  }

  /**
   * Test the queue counter by deleting items and emptying the queue.
   */
  public function testQueueCountBehavior(): void {
    // The count is not reliable.
  }

  /**
   * Test that createQueue() doesn't empty the queue if already created.
   */
  public function testCreateQueue(): void {
    $this->queue->createItem([1, 2, 3]);
    $this->queue->createQueue();
    $this->assertEquals(1, $this->queue->numberOfItems());

    $this->queue->deleteQueue();
  }

  /**
   * Test creating, claiming and releasing of items.
   */
  public function testCreatingClaimingAndReleasing(): void {
    $this->queue->createItem([1, 2, 3]);
    $claim = $this->queue->claimItem(3600);
    // Change the claim data to verify that releasing changed data, persists.
    $this->assertIsObject($claim);
    $claim->data = [4, 5, 6];
    $this->assertFalse($this->queue->claimItem(3600));
    $this->assertTrue($this->queue->releaseItem($claim));
    $this->assertTrue(is_object($claim = $this->queue->claimItem(3600)));
    $this->assertIsObject($claim);
    $this->assertSame($claim->data, [4, 5, 6]);
    $this->queue->releaseItem($claim);
    $this->assertSame(
      count($this->queue->createItemMultiple([1, 2, 3, 4])),
      4
    );
    $claims = $this->queue->claimItemMultiple(5, 3600);
    $this->assertIsArray($claims);
    foreach ($claims as $i => $claim) {
      // @phpstan-ignore-next-line
      $this->assertIsObject($claim);
      $claim->data = 9;
      $claims[$i] = $claim;
    }
    $this->assertSame($this->queue->claimItemMultiple(5, 3600), []);
    $this->assertSame($this->queue->releaseItemMultiple($claims), []);
    $claims = $this->queue->claimItemMultiple(5, 3600);
    $this->assertIsArray($claims);
    $this->assertSame(count($claims), 5);
    foreach ($claims as $i => $claim) {
      // @phpstan-ignore-next-line
      $this->assertIsObject($claim);
      $this->assertEquals(9, $claim->data);
    }

    $this->queue->deleteQueue();
  }

  /**
   * Test the behavior of lease time when claiming queue items.
   */
  public function testLeaseTime(): void {
    $this->assertFalse($this->queue->claimItem());
    $this->queue->createItem($this->randomString());
    $this->assertEquals(1, $this->queue->numberOfItems());
    $this->assertTrue(is_object($this->queue->claimItem(5)));
    $this->assertFalse($this->queue->claimItem());
    sleep(6);
    $this->assertTrue(is_object($this->queue->claimItem(2)));
    $this->assertFalse($this->queue->claimItem(1));
    sleep(3);
    $this->assertTrue(is_object($this->queue->claimItem(2)));
    $this->queue->deleteQueue();

    // Test claimItemMultiple which should work in the same way.
    $this->assertTrue(empty($this->queue->claimItemMultiple(2)));
    for ($i = 1; $i <= 5; $i++) {
      $this->queue->createItem($this->randomString());
    }
    $this->assertSame(count($this->queue->claimItemMultiple(5, 5)), 5);
    $this->assertTrue(empty($this->queue->claimItemMultiple(2)));
    sleep(6);
    $this->assertSame(count($this->queue->claimItemMultiple(5, 5)), 5);

    $this->queue->deleteQueue();
  }

}
