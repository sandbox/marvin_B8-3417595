<?php

namespace Drupal\Tests\purger_extended_queues\Kernel\Queue;

use Drupal\Core\Site\Settings;

/**
 * Tests \Drupal\purge\Plugin\Purge\Queue\DatabaseQueue.
 *
 * @group purger_extended_queues
 */
class RedisQueueTest extends PluginTestBase {

  /**
   * {@inheritDoc}
   */
  public function testPaging(): void {
    // Not supported.
  }

  /**
   * {@inheritDoc}
   */
  public function setUp($switch_to_memory_queue = TRUE): void {
    parent::setUp($switch_to_memory_queue);
    $settings = Settings::getAll();
    $settings['redis.connection'] = [];
    $settings['redis.connection']['host'] = 'redis';
    $settings['redis.connection']['port'] = 6379;
    $settings['redis.connection']['interface'] = 'PhpRedis';
    new Settings($settings);
    try {
      $this->setUpQueuePlugin();
      $this->queue->numberOfItems();
    }
    catch (\Throwable $e) {
      echo $e->getMessage();
      $this->markTestSkipped('requires redis');
    }
  }

  /**
   * {@inheritDoc}
   */
  protected static $modules = ['purge', 'purger_extended_queues', 'redis'];

  /**
   * {@inheritdoc}
   */
  protected $pluginId = 'redis';

}
