<?php

namespace Drupal\purger_extended_queues\Redis;

use Drupal\redis\Queue\ReliablePredis;

/**
 * A wrapper class for ReliablePredis.
 */
class PurgePredis extends ReliablePredis implements PurgeExtensionInterface {
  use RedisPurgerTrait;

  /**
   * {@inheritdoc}
   */
  public function releaseItem($item) {
    $this->client->pipeline()
      ->lrem($this->claimedListKey, -1, $item->item_id)
      ->lpush($this->availableListKey, $item->item_id)
      ->hset($this->availableItems, $item->item_id, serialize($item))
      ->execute();
    return TRUE;
  }

}
