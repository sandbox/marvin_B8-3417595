<?php

namespace Drupal\purger_extended_queues\Redis;

use Drupal\redis\Queue\ReliablePhpRedis;

/**
 * A wrapper class for ReliablePhpRedis.
 */
class PurgePhpRedis extends ReliablePhpRedis implements PurgeExtensionInterface {
  use RedisPurgerTrait;

  /**
   * {@inheritdoc}
   */
  public function releaseItem($item) {
    // @phpstan-ignore-next-line
    $this->client->multi()
      ->lrem($this->claimedListKey, $item->item_id, -1)
      ->lpush($this->availableListKey, $item->item_id)
      ->hset($this->availableItems, $item->item_id, serialize($item))
      ->exec();
    return TRUE;
  }

}
