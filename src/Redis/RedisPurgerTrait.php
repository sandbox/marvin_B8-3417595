<?php

namespace Drupal\purger_extended_queues\Redis;

/**
 * Adds default selectPage* method implementations to queue implementations.
 */
trait RedisPurgerTrait {

  /**
   * The configured limit of items on selected data pages.
   *
   * @var int
   */
  protected $selectPageLimit = 15;

  /**
   * {@inheritdoc}
   */
  public function selectPageLimit($set_limit_to = NULL) {
    if (is_int($set_limit_to)) {
      $this->selectPageLimit = $set_limit_to;
    }
    return $this->selectPageLimit;
  }

  /**
   * {@inheritDoc}
   */
  public function createItemMultiple(array $items) {
    $response = [];
    foreach ($items as $item) {
      $response[] = $this->createItem($item);
    }
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function claimItemMultiple($claims = 10, $lease_time = 3600) {
    $items = [];
    for ($i = 0; $i <= $claims; $i++) {
      $item = $this->claimItem($lease_time);
      if (!empty($item)) {
        $items[] = $item;
      }
    }
    // @phpstan-ignore-next-line
    return $items;
  }

  /**
   * {@inheritDoc}
   */
  public function deleteItemMultiple(array $items) {
    foreach ($items as $item) {
      $this->deleteItem($item);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function releaseItemMultiple(array $items) {
    foreach ($items as $item) {
      $this->releaseItem($item);
    }
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function selectPageMax() {
    // @phpstan-ignore-next-line
    $max = ((int) $this->client->lLen($this->availableListKey)) / $this->selectPageLimit();
    return intval(ceil($max));
  }

  /**
   * {@inheritDoc}
   */
  public function selectPage($page = 1) {
    if (($page < 1) || !is_int($page)) {
      throw new \LogicException('Parameter $page has to be a positive integer.');
    }

    $items = [];
    $limit = $this->selectPageLimit();

    // @phpstan-ignore-next-line
    $resultset = $this->client->lRange($this->availableListKey, (($page - 1) * $limit), ($limit - 1 + (($page - 1) * $limit)));
    // @phpstan-ignore-next-line
    $resultset = $this->client->hMGet($this->availableItems, $resultset);

    if (empty($resultset)) {
      return [];
    }

    foreach ($resultset as $key => $item) {
      $element = unserialize($item);
      if (!$item) {
        continue;
      }
      $items[] = $element;
    }

    return $items;
  }

}
