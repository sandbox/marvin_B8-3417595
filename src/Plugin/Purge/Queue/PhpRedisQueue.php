<?php

namespace Drupal\purger_extended_queues\Plugin\Purge\Queue;

use Drupal\purge\Plugin\Purge\Queue\QueueInterface;
use Drupal\purger_extended_queues\Redis\PurgeExtensionInterface;
use Drupal\purger_extended_queues\Redis\RedisPurgerQueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A QueueInterface compliant for redis usage queue.
 *
 * WARNING: This is only a simple adapter.
 *
 * @PurgeQueue(
 *   id = "redis",
 *   label = @Translation("Redis"),
 *   description = @Translation("A persistent, reliable redis queue (useful on production systems)."),
 * )
 */
class PhpRedisQueue implements QueueInterface {
  const QUEUE_NAME = "purge_queue";

  /**
   * The active implementation.
   *
   * @var \Drupal\purger_extended_queues\Redis\PurgeExtensionInterface
   */
  protected $implementation;

  /**
   * The constructor.
   *
   * @param \Drupal\purger_extended_queues\Redis\PurgeExtensionInterface $implementation
   *   The choosen implementation.
   */
  public function __construct(PurgeExtensionInterface $implementation) {
    $this->implementation = $implementation;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    // @phpstan-ignore-next-line
    if (!$container->has('redis.factory')) {
      throw new \Exception('Please install the redis module.');
    }

    /** @var \Drupal\redis\ClientFactory $clientFactory */
    $clientFactory = $container->get('redis.factory');
    /** @var \Drupal\Core\Site\Settings $settings */
    $settings = $container->get('settings');

    $redis = new RedisPurgerQueueFactory($clientFactory, $settings);
    $implementation = $redis->get(self::QUEUE_NAME);
    return new static(
      $implementation
    );
  }

  /**
   * {@inheritDoc}
   */
  public function createItemMultiple(array $items) {
    return $this->implementation->createItemMultiple($items);
  }

  /**
   * {@inheritDoc}
   */
  public function claimItemMultiple($claims = 10, $lease_time = 3600) {
    return $this->implementation->claimItemMultiple($claims, $lease_time);
  }

  /**
   * {@inheritDoc}
   */
  public function deleteItemMultiple(array $items) {
    return $this->implementation->deleteItemMultiple($items);
  }

  /**
   * {@inheritDoc}
   */
  public function releaseItemMultiple(array $items) {
    return $this->implementation->releaseItemMultiple($items);
  }

  /**
   * {@inheritdoc}
   */
  public function numberOfItems() {
    return $this->implementation->numberOfItems();
  }

  /**
   * {@inheritDoc}
   */
  public function selectPage($page = 1) {
    return $this->implementation->selectPage($page);
  }

  /**
   * {@inheritDoc}
   */
  public function selectPageMax() {
    return $this->implementation->selectPageMax();
  }

  /**
   * {@inheritDoc}
   */
  public function deleteItem($item) {
    $this->implementation->deleteItem($item);
  }

  /**
   * {@inheritDoc}
   */
  public function selectPageLimit($set_limit_to = NULL) {
    return $this->implementation->selectPageLimit($set_limit_to);
  }

  /**
   * {@inheritDoc}
   */
  public function createItem($data) {
    return $this->implementation->createItem($data);
  }

  /**
   * {@inheritDoc}
   */
  public function claimItem($lease_time = 3600) {
    return $this->implementation->claimItem($lease_time);
  }

  /**
   * {@inheritDoc}
   */
  public function releaseItem($item) {
    return $this->implementation->releaseItem($item);
  }

  /**
   * {@inheritDoc}
   */
  public function createQueue() {
    $this->implementation->createQueue();
  }

  /**
   * {@inheritDoc}
   */
  public function deleteQueue() {
    $this->implementation->deleteQueue();
  }

}
