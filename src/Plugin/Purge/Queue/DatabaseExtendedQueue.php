<?php

namespace Drupal\purger_extended_queues\Plugin\Purge\Queue;

use Drupal\purge\Plugin\Purge\Queue\DatabaseQueue;

/**
 * A QueueInterface compliant database backed queue.
 *
 * @PurgeQueue(
 *   id = "database_extended",
 *   label = @Translation("Database Extended"),
 *   description = @Translation("A scalable database backed queue."),
 * )
 */
class DatabaseExtendedQueue extends DatabaseQueue {

  /**
   * {@inheritdoc}
   */
  protected function ensureTableExists() {
    // Wrap ::ensureTableExists() to prevent expensive duplicate code paths.
    if (!$this->tableExists) {

      $database_schema = $this->connection->schema();

      if ($database_schema->tableExists(self::TABLE_NAME)) {
        $this->tableExists = TRUE;
        return TRUE;
      }

      if (parent::ensureTableExists()) {
        $this->tableExists = TRUE;
        return TRUE;
      }
    }
    return $this->tableExists;
  }

}
