<?php

namespace Drupal\purger_extended_queues\Plugin\Purge\Queue;

use Drupal\purge\Plugin\Purge\Queue\QueueBasePageTrait;
use Drupal\purge\Plugin\Purge\Queue\QueueInterface;
use Drupal\rabbitmq\Queue\Queue;
use Drupal\rabbitmq\Queue\QueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A QueueInterface compliant volatile memory buffer queue.
 *
 * @PurgeQueue(
 *   id = "rabbitmq",
 *   label = @Translation("RabbitMQ"),
 *   description = @Translation("A non-persistent, per-request memory queue (not useful on production systems)."),
 * )
 */
class RabbitmqQueue extends Queue implements QueueInterface {
  use QueueBasePageTrait;

  const QUEUE_NAME = "purge_queue";

  /**
   * {@inheritDoc}
   */
  // phpcs:disable
  public function createQueue() {
    parent::createQueue();
  }
  // phpcs:enable

  /**
   * {@inheritDoc}
   */
  public function createItem($data):bool {
    // @phpstan-ignore-next-line
    return parent::createItem(serialize(['package' => $data]));
  }

  /**
   * {@inheritDoc}
   */
  public function claimItem($lease_time = 3600) {
    $item = parent::claimItem($lease_time);
    // @phpstan-ignore-next-line
    $item->data = unserialize($item->data)['package'];
    return $item;
  }

  /**
   * {@inheritDoc}
   */
  public function releaseItem($item): bool {

    /** @var \PhpAmqpLib\Message\AMQPMessage $message */
    $message = $this->messages[$item->id];
    /** @var \PhpAmqpLib\Channel\AMQPChannel $channel */
    $channel = $message->getChannel();
    $channel->basic_ack($message->getDeliveryTag(), FALSE);
    unset($this->messages[$item->id]);

    $this->createItem($item->data);

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function createItemMultiple(array $items) {
    $response = [];
    foreach ($items as $item) {
      $response[] = $this->createItem($item);
    }
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function claimItemMultiple($claims = 10, $lease_time = 3600) {
    $items = [];
    for ($i = 0; $i <= $claims; $i++) {
      $item = $this->claimItem($lease_time);
      if ($item) {
        $items[] = $item;
      }
    }
    // @phpstan-ignore-next-line
    return $items;
  }

  /**
   * {@inheritDoc}
   */
  public function deleteItemMultiple(array $items) {
    foreach ($items as $item) {
      $this->deleteItem($item);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function releaseItemMultiple(array $items) {
    foreach ($items as $item) {
      $this->releaseItem($item);
    }
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function selectPage($page = 1) {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    $configFactory = $container->get('config.factory');
    $moduleConfig = $configFactory->get(QueueFactory::MODULE_CONFIG);
    return new static(
      self::QUEUE_NAME,
      $container->get('rabbitmq.connection.factory'),
      $container->get('module_handler'),
      $container->get('logger.channel.rabbitmq'),
      $moduleConfig
    );

  }

}
